﻿// Unsolved Case: Illegal start by Os. Where Os went first && game is unfinished && the number of Os and Xs is equal
//                there is no way to know who's turn it is next

// Unsolved Case: Illegal move by either player during the course of a game that leaves the balance of X and O correct

// Partially Solved: O_DISQUALIFIED_WENT_FIRST_OR_EXTRA_MOVES
// No way to know which illegal move was made

// Assumtion: UNFINNISHED_X_TURN_NEXT, UNFINNISHED_O_TURN_NEXT
// Games that are unfinished were played legally up to the current point

// Assumtion: NOUGHTS_WIN, CROSSES_WIN, DRAW
// Games that are completed were played legally

using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace myApp
{
    class Program
    {
        private enum BoardState
        {
            NOUGHTS_WIN, 
            CROSSES_WIN, 
            DRAW, 
            UNFINNISHED_X_TURN_NEXT,
            UNFINNISHED_O_TURN_NEXT,
            X_DISQUALIFIED_FOR_EXTRA_MOVES,
            O_DISQUALIFIED_FOR_EXTRA_MOVES,
            O_DISQUALIFIED_WENT_FIRST_OR_EXTRA_MOVES,
            DISQUALIFIED_TWO_WINNERS,
            NOT_A_VALID_BOARD,
            ERROR_NO_BOARD,
        }

        private static Boolean validateBoard(string board) 
        {
            if(board.Length != 9) {
                return false;
            }

            string pattern = @"[^XO_]";
            if(Regex.IsMatch(board, pattern)) {
                return false;
            }
            
            return true;
        }

        private static int countSquares(string board, char symbol) 
        {
            int symbolCount = 0;
            foreach ( char c in board )
            {
                if (c == symbol) 
                {
                    symbolCount ++;
                }
            }
            return symbolCount;
        }

        private static string[] getRows(string board)
        {
            string[] rows = new string[3];
            string temp = "";
            int countRow = 0;
            int countLine = 0;
            foreach (char c in board) {
                temp += c;
                countRow ++;
                if(countRow == 3) {                    
                    rows[countLine] = temp;
                    countLine ++;
                    temp = "";
                    countRow = 0;
                }
            }
            return rows;
        }

        private static string[] getColumns(string board)
        {
            string[] columns = new string[3];
            string col1 = "";
            string col2 = "";
            string col3 = "";
            int countColum = 0;

            while (countColum <= board.Length - 1) {
                foreach (char c in board) {
                    if(countColum == 0 || countColum == 3 || countColum == 6) {
                        col1 += c;
                        countColum ++;
                    }
                    else if (countColum == 1 || countColum == 4 || countColum == 7)
                    {
                        col2 += c;
                        countColum ++;
                    }
                    else
                    {
                        col3 += c;
                        countColum ++;
                    }
                }
            }
            columns[0] = col1;
            columns[1] = col2;
            columns[2] = col3;                       
            return columns;
        }

        private static string[] getDiagonals(string board)
        {
            string[] diagonals = new string[2];
            string dia1 = "";
            string dia2 = "";
            int countDiag = 0;
                
            while (countDiag <= board.Length - 1)
            {
                foreach (char c in board)
                {
                    if( countDiag == 4 ) {
                    dia1 += c;
                    dia2 += c;
                    countDiag ++;
                }
                else if (countDiag == 0 || countDiag == 8)
                {
                    dia1 += c;
                    countDiag ++;
                }
                else if (countDiag == 2 || countDiag == 6)
                {
                    dia2 += c;
                    countDiag ++;
                }
                else
                {
                    countDiag++;
                }
                } 
                
            }
            diagonals[0] = dia1;
            diagonals[1] = dia2;                                  
            return diagonals;
        }

         private static int checkCheating(String[] lines, int Xs, int Os, int Spaces, Boolean XWin, Boolean OWin)
        {
            int cheatCode = 0;

            // Check Xs went first and Os haven't made extra moves
            if(Os == Xs + 1) {
                cheatCode = 1;
            }
            // Check number of moves for cheating
            if(Os > Xs) {
                cheatCode = 2;
            } else if(Xs >= Os + 2) {
                cheatCode = 3;
            }

            // Check the game is finished
            if(XWin == false && OWin == false && Spaces != 0 && Xs == Os) {
                cheatCode = 4;
            }

            if(XWin == false && OWin == false && Spaces != 0 && Xs > Os) {
                cheatCode = 5;
            }

            // Check there is a single winner
            if(XWin == true && OWin == true) {
                cheatCode = 6;
            }      
            return cheatCode;
        }

        private static Boolean setWinner(string[] lines, string combo)
        {
            Boolean winner = false;
             
            foreach(var item in lines) {
                if(item == combo ) {
                    winner = true;
                }
            }
            return winner;
        }

        private static BoardState checkWinner(Boolean XWin, Boolean OWin)
        {
            
            // Check for win by X
            if(XWin == true) {
                return BoardState.CROSSES_WIN;
            }

            // Check for win by O
            if(OWin == true) {
                return BoardState.NOUGHTS_WIN;
            }
            return BoardState.DRAW;
        }

        private static BoardState GetStateOfBoard(string board)
        {  
               
            // Check if board is valid
            Boolean validBoard = validateBoard(board);
            if(validBoard == false) 
            {
                return BoardState.NOT_A_VALID_BOARD;
            }

            // Count Xs Os and Spaces
            int Xs = countSquares(board, 'X');
            int Os = countSquares(board, 'O');
            int Spaces = countSquares(board, '_');

            // Array of possible winning lines
            string[] lines = getRows(board).Concat(getColumns(board)).Concat(getDiagonals(board)).ToArray();

            // Set the winner
            Boolean XWin = setWinner(lines, "XXX");
            Boolean OWin = setWinner(lines, "OOO");

            // Check for cheating
            int cheatCode = checkCheating(lines, Xs, Os, Spaces, XWin, OWin);

            if (cheatCode == 1)
            {
                return BoardState.O_DISQUALIFIED_WENT_FIRST_OR_EXTRA_MOVES;
            }
            
            if (cheatCode == 2)
            {
                return BoardState.O_DISQUALIFIED_FOR_EXTRA_MOVES;
            }

            if (cheatCode == 3)
            {
                return BoardState.X_DISQUALIFIED_FOR_EXTRA_MOVES;
            }

            if (cheatCode == 4)
            {
                return BoardState.UNFINNISHED_X_TURN_NEXT;
            }       

            if (cheatCode == 5)
            {
                return BoardState.UNFINNISHED_O_TURN_NEXT;
            }

            if (cheatCode == 6)
            {
                return BoardState.DISQUALIFIED_TWO_WINNERS;
            }

            // Check for winner or draw
            return checkWinner(XWin, OWin);
        }

        static void Main(string[] args)
        {   
            for (int i = 0; i < args.Length; i++)
            {
            System.Console.WriteLine(GetStateOfBoard(args[i]));
            }
        }
    }
}
