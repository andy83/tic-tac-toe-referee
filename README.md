### Tic Tac Toe Referee

A console app to check the winner of a game of tic tac toe.

Enter a 9 digit string that represents a tic tac toe board using X O and _ for empty spaces.

The string is case sensitive

dotnet run XXXOXOO__

## Rules

X's must go first
